depth_track

reader: fast sequence reader api to change dataset easily

calibration : module for undistortion, 3d cloud generation and reprojection of any 3d point (in our case KFilter output) to sensor scene

mask : binary processing module to generate important pixels given conf and depth image pair

cluster : clustering module to generate arbitrary number of regions given important pixels

tracker : track creation, assignment, update and deletion module. tracks 3d pointer with KF based prediction and correction iteration. also recognizes gestures based on a basic decision tree. multiple tracks handled via hungarian algorithm.

munkres : 3rd party lib for hungarian algo

recognizer : 3rd party lib for recognition




