#include "tracker.hpp"

Track Tracker::getCandidate(const cv::Mat & depth_undistorted,
                            const cv::Mat & point_cloud,
                            const std::vector<cv::Point> & cluster)
{
    Track candidate;


    candidate.isAssigned = false;
    candidate.isGestureStarted = false;
    candidate.state = Track::Candidate;

    // id
    candidate.id = -1;

    // 2D mask
    candidate.mask = cv::Mat::zeros(depth_undistorted.size(), CV_8UC1);
    for(const auto & p : cluster) { candidate.mask.ptr<uchar>(p.y)[p.x] = 255; }

    // counters
    candidate.history = 0;
    candidate.lostCount = 0;

    // rects
    candidate.boundRect = cv::boundingRect(cluster);
    candidate.minRect = cv::minAreaRect(cluster);

    // depth histogram
    int d_bins = 500;
    int histSize[] = { d_bins };
    float d_ranges[] = { 0, m_params->maxDepth };
    const float * ranges[] = { d_ranges };
    int channels[] = { 0 };
    cv::calcHist(&depth_undistorted, 1, channels, candidate.mask, candidate.histogram, 1, histSize, ranges);
    cv::normalize(candidate.histogram, candidate.histogram, 0, 1, cv::NORM_MINMAX, -1, cv::Mat());

    // mean depth
    candidate.meanDepth = cv::mean(depth_undistorted, candidate.mask)[0];

    // min width
    candidate.minWidth = INT_MAX;
    cv::Point2f points[4];
    candidate.minRect.points(points);
    for(int i = 0; i < 3; ++i) {
        cv::Point3f p1 = m_calibration->get3D(points[i].x, points[i].y, candidate.meanDepth);
        cv::Point3f p2 = m_calibration->get3D(points[i + 1].x, points[i + 1].y, candidate.meanDepth);
        double dist_sqr = Calibration::getDistanceSqr(p1, p2);
        if(dist_sqr < candidate.minWidth)
            candidate.minWidth = dist_sqr;
    }
    candidate.minWidth = sqrt(candidate.minWidth);

    // moments, hull and mean position
    std::vector<std::vector<cv::Point> > contours;
    cv::Mat mask_dilated;
    cv::dilate(candidate.mask, mask_dilated, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5), cv::Point(2, 2)));
    cv::findContours(mask_dilated, contours, CV_RETR_LIST, cv::CHAIN_APPROX_SIMPLE);

    if(contours.size() > 0) {
        std::sort(contours.begin(), contours.end(), [&] (std::vector<cv::Point> & c1, std::vector<cv::Point> & c2) {
            return cv::contourArea(c1) > cv::contourArea(c2);
        });
        candidate.contour = contours[0];
    }
    cv::convexHull(cluster, candidate.hull);
    candidate.moments = cv::moments(candidate.hull);
    candidate.area = cv::contourArea(candidate.hull);
    candidate.position_2d = cv::Point2f(candidate.moments.m10 / candidate.moments.m00, candidate.moments.m01 / candidate.moments.m00);
    candidate.position_3d = m_calibration->get3D(candidate.position_2d.x, candidate.position_2d.y, candidate.meanDepth);
    // std::cout << candidate.position_3d.x << " "<<  candidate.position_3d.y <<" "<< candidate.position_3d.z << " cp" << std::endl;

    // pointer
    double min, max;
    cv::Point minLoc, maxLoc;
    cv::minMaxLoc(depth_undistorted, &min, &max, &minLoc, &maxLoc, candidate.mask);
    cv::Vec3s val = point_cloud.at<cv::Vec3s>(minLoc);

    candidate.pointer_2d = minLoc;
    candidate.pointer_3d = cv::Point3f(val[0], val[1], val[2]);
    // m_calibration->get3D(minLoc.x, minLoc.y, candidate.meanDepth);

    /*
     * kalman filter start with initial position
     * constant accel transition matrix for 3d
     */
    float dt = 1. / m_params->fps;
    float v = dt;
    float a = 0.5 * dt * dt;
    candidate.kf_3d = cv::KalmanFilter(9, 3, 0, CV_32F);
    candidate.kf_3d.transitionMatrix = (cv::Mat_<float>(9, 9) <<
                                        1, 0, 0, v, 0, 0, a, 0, 0,
                                        0, 1, 0, 0, v, 0, 0, a, 0,
                                        0, 0, 1, 0, 0, v, 0, 0, a,
                                        0, 0, 0, 1, 0, 0, v, 0, 0,
                                        0, 0, 0, 0, 1, 0, 0, v, 0,
                                        0, 0, 0, 0, 0, 1, 0, 0, v,
                                        0, 0, 0, 0, 0, 0, 1, 0, 0,
                                        0, 0, 0, 0, 0, 0, 0, 1, 0,
                                        0, 0, 0, 0, 0, 0, 0, 0, 1);
    candidate.kf_3d.statePre.at<float>(0) = candidate.pointer_3d.x;
    candidate.kf_3d.statePre.at<float>(1) = candidate.pointer_3d.y;
    candidate.kf_3d.statePre.at<float>(2) = candidate.pointer_3d.z;
    candidate.kf_3d.statePre.at<float>(3) = 0;
    candidate.kf_3d.statePre.at<float>(4) = 0;
    candidate.kf_3d.statePre.at<float>(5) = 0;
    candidate.kf_3d.statePre.at<float>(6) = 0;
    candidate.kf_3d.statePre.at<float>(7) = 0;
    candidate.kf_3d.statePre.at<float>(8) = 0;
    cv::setIdentity(candidate.kf_3d.measurementMatrix);
    cv::setIdentity(candidate.kf_3d.processNoiseCov, cv::Scalar::all(m_params->processNoise));
    cv::setIdentity(candidate.kf_3d.measurementNoiseCov, cv::Scalar::all(m_params->measurementNoise));
    cv::setIdentity(candidate.kf_3d.errorCovPost, cv::Scalar::all(m_params->errorCovariance));
    candidate.measurement_3d = cv::Mat(3, 1, CV_32F);
    candidate.measurement_3d.at<float>(0) = candidate.pointer_3d.x;
    candidate.measurement_3d.at<float>(1) = candidate.pointer_3d.y;
    candidate.measurement_3d.at<float>(2) = candidate.pointer_3d.z;
    candidate.kf_3d.predict();
    candidate.kf_3d.correct(candidate.measurement_3d);
    candidate.kf_3d.predict();
    candidate.kf_3d.correct(candidate.measurement_3d);

    return candidate;
}

void Tracker::run(const cv::Mat & depth_undistorted,
                  const cv::Mat & point_cloud,
                  const std::vector<std::vector<cv::Point> > & clusters)
{
    std::vector<Track> candidates;
    std::map<int, int> assignments;
    cv::Mat_<int> costMatrix;

    /* predict next positions, and set all tracks unassigned */
    for(auto & T : tracks) {
        Track & track = T.second;

        track.prediction_3d = track.kf_3d.predict();
        track.isAssigned = false;
    }

    /* create candidate tracks */
    for(auto & C : clusters) candidates.emplace_back(getCandidate(depth_undistorted, point_cloud, C));

    /*
     * 1 - association
     *
     */

    /* prepare cost matrix */
    if(tracks.size() && candidates.size()) {
        costMatrix = cv::Mat_<int>(clusters.size(), tracks.size());
    }
    for(int i = 0; i < candidates.size(); ++i) {
        Track & candidate = candidates[i];

        for(auto it = tracks.begin(); it != tracks.end(); ++it) {
            int j = std::distance(tracks.begin(), it);
            Track & track = it->second;

            /* populate cost matrix */
            cv::Point3f pr_pos;
            pr_pos.x = track.prediction_3d.at<float>(0);
            pr_pos.y = track.prediction_3d.at<float>(1);
            pr_pos.z = track.prediction_3d.at<float>(2);
            if(track.history <= 2)
                pr_pos = track.pointer_3d;

            double p_hist = cv::compareHist(track.histogram, candidate.histogram, CV_COMP_CORREL);
            double p_dist = sqrt(Calibration::getDistanceSqr(pr_pos, candidate.pointer_3d));

            // TODO normalize beforehand this is ugly
            //
            costMatrix(i, j) = - (100. * p_hist) - (10000. / p_dist);
        }
    }

    /* run hungarian algorithm to get assignments */
    Munkres munkres;
    munkres.solve(costMatrix);
    for(int i = 0; i < costMatrix.rows; ++i) {
        for(int j = 0; j < costMatrix.cols; ++j) {
            if(costMatrix.at<float>(i, j) == 0) {
                assignments.insert(std::make_pair(i, j));
            }
        }
    }

    /*
     * 1.1 - update
     *
     */

    for(auto & A : assignments) {
        Track & candidate = candidates[A.first];

        auto it = tracks.begin();
        std::advance(it, A.second);
        Track & track = it->second;

        cv::Point3f pos_3d;
        pos_3d.x = track.prediction_3d.at<float>(0);
        pos_3d.y = track.prediction_3d.at<float>(1);
        pos_3d.z = track.prediction_3d.at<float>(2);
        if(track.history <= 2) {
            pos_3d = track.pointer_3d;
        }

        double p_dist = sqrt(Calibration::getDistanceSqr(pos_3d, candidate.pointer_3d));
        // std::cout << track.id << " " << pos_3d.x << " "<<  pos_3d.y <<" "<< pos_3d.z << " tp" << std::endl;
        // std::cout << candidate.pointer_3d.x << " " <<  candidate.pointer_3d.y <<" "<< candidate.pointer_3d.z << " cp" << std::endl;

        /* too far to assign */
        if((track.lostCount == 0 && p_dist > m_params->searchRadius) ||
                (track.lostCount > 0 && p_dist > track.lostCount * m_params->searchRadius))
            continue;

        // std::cout << track.id << " is assigned " << p_dist <<std::endl;

        candidate.isAssigned = true;
        track.isAssigned = true;

        track.history++;
        if(track.history < 10) track.state = Track::New;
        else track.state = Track::Verified;
        track.lostCount = 0;

        track.mask = candidate.mask;
        track.histogram = candidate.histogram;
        track.contour = candidate.contour;
        track.hull = candidate.hull;
        track.boundRect = candidate.boundRect;
        track.meanDepth = candidate.meanDepth;
        track.minRect = candidate.minRect;
        track.minWidth = candidate.minWidth;
        track.moments = candidate.moments;
        track.position_2d = candidate.position_2d;
        track.position_3d = candidate.position_3d;

        // estimate 3d pointer pos
        track.measurement_3d = candidate.measurement_3d.clone();
        track.estimation_3d = track.kf_3d.correct(track.measurement_3d);
        track.pointer_3d.x = track.estimation_3d.at<float>(0);
        track.pointer_3d.y = track.estimation_3d.at<float>(1);
        track.pointer_3d.z = track.estimation_3d.at<float>(2);

        // reproj
        track.pointer_2d = m_calibration->get2D(track.pointer_3d.x, track.pointer_3d.y, track.pointer_3d.z);
        track.pointerHistory.push_back(track.pointer_2d);
    }

    // update unassigned tracks
    for(auto & T : tracks) {
        if(T.second.isAssigned) continue;

        Track & track = T.second;
        track.history++;
        track.lostCount++;
        track.state = Track::Lost;

        track.estimation_3d = track.kf_3d.correct(track.measurement_3d);
        track.pointer_3d.x = track.estimation_3d.at<float>(0);
        track.pointer_3d.y = track.estimation_3d.at<float>(1);
        track.pointer_3d.z = track.estimation_3d.at<float>(2);

        // reproj
        track.pointer_2d = m_calibration->get2D(track.pointer_3d.x, track.pointer_3d.y, track.pointer_3d.z);
        track.pointerHistory.push_back(track.pointer_2d);
    }


    /*
     * 2 - initiate unassigned candidates
     *
     */

    for(auto & C : candidates) {
        /* unassigned only */
        if(C.isAssigned) continue;

        //if(tracks.size() >= m_params->maxCount) continue;

        if(C.minWidth > m_params->minWidth && C.minWidth < m_params->maxWidth && C.area > m_params->minArea)
        {
            m_lastTrackId++;
            C.id = m_lastTrackId;
            C.isAssigned = true;
            //std::cout << m_lastTrackId << " is inserted" << std::endl;

            tracks.insert(std::make_pair(C.id, C));
        }
    }


    /*
     * 3 - delete bad tracks
     * rule 1 : lost
     * rule 2 : not moving
     * rule 3 : delete all but params.maxCount
     *
     */

    for(auto it = tracks.begin(); it != tracks.end();) {
        // if(it->second.isAssigned) { it++; continue; }

        Track & track = it->second;

        // rule 1 - lost count
        bool isLost = track.lostCount > m_params->maxFramesLost;

        // rule 2 - not moving
        bool isStill = false;
        if(track.history > 2 * m_params->fps) {
            std::vector<cv::Point2f> historySample;
            auto it_hist = track.pointerHistory.end() - 2 * m_params->fps;
            historySample.insert(historySample.end(), it_hist, track.pointerHistory.end());
            cv::Rect rect = cv::boundingRect(historySample);
            isStill = std::max(rect.width, rect.height) < 20;
        }

        if(isLost || isStill) tracks.erase(it++);

        else it++;
    }

    // rule 3 - garbage collector

    /* assign probabilities to tracks */
    std::deque<int> t_ids;
    for(auto & T : tracks) {
        Track & track = T.second;

        track.prob = 0;
//        track.prob -= (2 * track.lostCount);

        double ar;
        if(track.history > 2 * m_params->fps) {
            std::vector<cv::Point2f> historySample;
            auto it_hist = track.pointerHistory.end() - 2 * m_params->fps;
            historySample.insert(historySample.end(), it_hist, track.pointerHistory.end());
            cv::Rect rect = cv::boundingRect(historySample);
            ar = std::max(rect.width, rect.height);
        }
        else {
            ar = 10;
        }
        double md = track.meanDepth;

        // TODO normalize
        //
        if(ar > 500.) ar = 500.;
        if(md < 100.) md = 100.;
        track.prob += ar / 10.;
        track.prob -= md / 100.;
        track.prob -= abs(track.area - (m_params->minArea + m_params->minArea * .5));

        t_ids.push_back(track.id);
    }
    std::sort(t_ids.begin(), t_ids.end(), [&] (const int &id1, const int &id2)
    { return tracks[id1].prob < tracks[id2].prob; });

    // delete all but maxFrameCount
    while(tracks.size() > m_params->maxTrackCount) {
        tracks.erase(t_ids[0]);
        t_ids.pop_front();
    }

    /*
     * 4 - gesture recognition from reprojected pointer (pointer_2d)
     *
     */
    for(auto & T : tracks) tryRecognize(T.second);

}

void Tracker::tryRecognize(Track & track)
{
    if(track.pointerHistory.size() < m_params->activityWindow) return;

    cv::Point2f p_new = track.pointerHistory[track.pointerHistory.size() - 1];

    std::vector<cv::Point2f> historySample;
    auto it_hist = track.pointerHistory.end() - m_params->activityWindow;
    historySample.insert(historySample.end(), it_hist, track.pointerHistory.end());
    cv::Rect rect = cv::boundingRect(historySample);

    double activity = std::max(rect.width, rect.height);
//    for(int i = track.pointerHistory.size() - m_params->activityWindow + 1; i < track.pointerHistory.size(); ++i) {
//        activity += sqrt(pow(track.pointerHistory[i].x - track.pointerHistory[i - 1].x, 2) + pow(track.pointerHistory[i].y - track.pointerHistory[i - 1].y, 2));
//    }

    if(activity > m_params->activityThresh) {
        std::cout << track.id << " ACT" << std::endl;
        if(!track.isGestureStarted) {
            std::cout << track.id << " G ST" << std::endl;
            track.isGestureStarted = true;
            track.gesturePoints.clear();
            track.gesturePoints.push_back(p_new);
        }
        else {
            track.gesturePoints.push_back(p_new);
        }
    }
    else {
        if(track.isGestureStarted) {
            if(track.gesturePoints.size() > m_params->minGesturePoints) {
                cv::Rect rect = cv::boundingRect(track.gesturePoints);
                if(std::max(rect.width, rect.height) > m_params->minGestureVertice) {
                    DollarRecognizer::Path2D path; path.reserve(track.gesturePoints.size());
                    DollarRecognizer::Path2D path_mirrored; path_mirrored.reserve(track.gesturePoints.size());
                    for(auto & p : track.gesturePoints) {
                        path.emplace_back(DollarRecognizer::Point2D(p.x, p.y));
                        path_mirrored.emplace_back(DollarRecognizer::Point2D(-p.x, p.y));
                    }
                    DollarRecognizer::RecognitionResult result = recognizer.recognize(path);
                    DollarRecognizer::RecognitionResult result_mirrored = recognizer.recognize(path_mirrored);
                    std::cout << track.id << " G TRY " << result.name << " " << result.score << std::endl;
                    std::cout << track.id << " G TRY " << result_mirrored.name << " " << result_mirrored.score << std::endl;
                    if(result.score > m_params->minGestureScore || result_mirrored.score > m_params->minGestureScore) {
                        std::cout << track.id << " G YES" << std::endl;
                        track.isGestureStarted = false;
                        if(result.score > result_mirrored.score) track.gestures.push_back(result);
                        else track.gestures.push_back(result_mirrored);
                        track.gesturePoints.clear();
                    }
                    else {
                        track.isGestureStarted = false;
                        track.gesturePoints.clear();
                        //track.gesturePoints.push_back(p_new);
                    }
                }
                else {
                    std::cout << track.id << " G END" << std::endl;
                    track.isGestureStarted = false;
                    track.gesturePoints.clear();
                }
            }
            else {
                track.gesturePoints.push_back(p_new);
            }
        }
    }

}
