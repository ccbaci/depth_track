TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -fopenmp
QMAKE_LFLAGS += -fopenmp

SOURCES += main.cpp \
    tracker.cpp \
    dollar_bw/GeometricRecognizer.cpp \
    munkres/munkres.cpp

unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += opencv

HEADERS += \
    mask.hpp \
    global.hpp \
    cluster.hpp \
    calibration.hpp \
    tracker.hpp \
    dollar_bw/GeometricRecognizer.h \
    dollar_bw/GeometricRecognizerTypes.h \
    dollar_bw/GestureTemplate.h \
    dollar_bw/PathWriter.h \
    dollar_bw/SampleGestures.h \
    munkres/munkres.h \
    reader.hpp
