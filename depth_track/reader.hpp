#ifndef READER_HPP
#define READER_HPP

#include <global.hpp>

/*
 * reader class to read the image sequences given dataset
 *
 *
 */

class Reader
{
public:
    Reader() = delete;
    Reader(const camera_type &type) {
        switch(type) {
        case ds325 : {
            m_path = "ds325/";
            m_extension = ".tiff";
            break;
        }
        case ds536 : {
            m_path = "ds536/";
            m_extension = ".tif";
            break;
        }
        }
    }

    void setRoot(const std::string & folderName) {
        m_root = folderName;
    }

    void selectDataset(const std::string & folderName) {
        m_dataset = folderName;
    }

    cv::Mat getDepth(int timestamp) {
        char number[7];
        std::snprintf(number, 7, "%06d", timestamp);
        std::string depth_path = m_root + m_path + m_dataset + number + "_depth" + m_extension;
        return cv::imread(depth_path, cv::IMREAD_UNCHANGED);
    }

    cv::Mat getConfidence(int timestamp) {
        char number[7];
        std::snprintf(number, 7, "%06d", timestamp);
        std::string conf_path = m_root + m_path + m_dataset + number + "_confidence" + m_extension;
        return cv::imread(conf_path, cv::IMREAD_UNCHANGED);
    }

private:
    std::string m_root;
    std::string m_path;
    std::string m_dataset;
    std::string m_extension;
};

#endif // READER_HPP
