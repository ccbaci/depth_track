#ifndef CALIBRATION_H
#define CALIBRATION_H

#include <global.hpp>

/*
 * calib class to undistort and transform images
 * with given camera parameters
 * also projects and reprojects 2d sensor <-> 3d
 *
 *
 */

class Calibration
{
public:
    Calibration() = delete;
    Calibration(const camera_type & type) {
        switch(type) {
        case ds325 : {
            m_data.fx = 224.5019989013672;
            m_data.fy = 230.4940032958984;
            m_data.k1 = -0.1701029986143112;
            m_data.k2 = 0.1440639942884445;
            m_data.p1 = 0;
            m_data.p2 = -0.04769939929246903;
            m_data.cx = 160;
            m_data.cy = 120;
            break;
        }
        case ds536 : {
            m_data.fx = 227.8414306640625;
            m_data.fy = 227.8155822753906;
            m_data.k1 = -0.1597864031791687;
            m_data.k2 = 0.09072628617286682;
            m_data.p1 = 0;
            m_data.p2 = 0;
            m_data.cx = 166.5474548339844;
            m_data.cy = 124.1132431030273;
            break;
        }
        }

        m_data.fx_inv = 1. / m_data.fx;
        m_data.fy_inv = 1. / m_data.fy;

        m_data.A = cv::Mat::zeros(cv::Size(3, 3), CV_32FC1);
        m_data.A.at<float>(0, 0) = m_data.fx; m_data.A.at<float>(1, 1) = m_data.fy;
        m_data.A.at<float>(0, 2) = m_data.cx; m_data.A.at<float>(1, 2) = m_data.cy;
        m_data.A.at<float>(2, 2) = 1.;

        m_data.K.clear();
        m_data.K.push_back(m_data.k1); m_data.K.push_back(m_data.k2);
        m_data.K.push_back(m_data.p1); m_data.K.push_back(m_data.p2);
    }

    /*
     * input distorted frame 16UC1
     * output undistorted frame 16UC1
     *
     */
    cv::Mat undistort(const cv::Mat & frame_distorted) const {
        cv::Mat frame_undistorted;
        cv::undistort(frame_distorted, frame_undistorted, m_data.A, m_data.K);
        return frame_undistorted;
    }

    /*
     * input undistorted frame 16UC1
     * calculate real world x, y, z using camera params and pinhole equation
     * output point cloud 16SC3
     *
     */
    inline cv::Mat getPointCloud(const cv::Mat & depth_undistorted) const {
        cv::Mat pcl = cv::Mat::zeros(depth_undistorted.size(), CV_16SC3);
#pragma omp for
        for(int i = 0; i < depth_undistorted.rows; ++i) {
            auto p = pcl.ptr<cv::Vec3s>(i);
            auto d = depth_undistorted.ptr<ushort>(i);

            for(int j = 0; j < depth_undistorted.cols; ++j) {

                p[j].val[0] = (j - m_data.cx) * d[j] * m_data.fx_inv;
                p[j].val[1] = (i - m_data.cy) * d[j] * m_data.fy_inv;
                p[j].val[2] = d[j];
            }
        }

        return pcl;
    }


    /*
     * utility functions
     *
     */

    // sensor + depth to 3D
    inline cv::Point3f get3D(int xp, int yp, double z) const {
        cv::Point3f p3D;
        p3D.x = (xp - m_data.cx) * z * m_data.fx_inv;
        p3D.y = (yp - m_data.cy) * z * m_data.fy_inv;
        p3D.z = z;
        return p3D;
    }

    // reprojection of 3D onto sensor
    inline cv::Point2f get2D(double x, double y, double z) const {
        cv::Point2f p2D;
        p2D.x = m_data.cx + x * m_data.fx / z;
        p2D.y = m_data.cy + y * m_data.fy / z;
        return p2D;
    }

    // squared distance between to points
    inline static double getDistanceSqr(const cv::Point3f & p1, const cv::Point3f & p2) {
        double dist;
        dist = pow((p1.x - p2.x), 2) +
                pow((p1.y - p2.y), 2) +
                pow((p1.z - p2.z), 2);
        return dist;
    }

    /* camera parameters */
    calib_data getData() {
        return m_data;
    }


private:
    calib_data m_data;

};

#endif // CALIBRATION_H
