#ifndef TRACKER_HPP
#define TRACKER_HPP

#include <global.hpp>
#include <calibration.hpp>
#include <munkres/munkres.h>
#include <dollar_bw/GeometricRecognizer.h>

/*
 * tracker class
 * assigns candidate tracks to old ones
 * update track positions based on 3d kalman filtering
 * initiates new tracks
 * deletes bad tracks
 * recognizes gestures based on a decision tree and $1 recognizer
 *
 * multiple object tracking handled - hungarian algorithm
 *
 */

struct Track {
    enum State {
        Candidate,
        New,
        Verified,
        Lost,
        Deleted
    } state;

    int id;

    uint history;
    uint lostCount;
    bool isAssigned;

    cv::Mat mask;
    std::vector<cv::Point> hull;
    cv::Mat histogram;

    // 2D features just in case
    // most of them not used now
    cv::Rect boundRect;
    cv::RotatedRect minRect;
    cv::Moments moments;
    std::vector<cv::Point> contour;
    double area;
    cv::Point position_2d;

    // 3D
    double minWidth;
    double meanDepth;
    cv::Point3f position_3d;
    cv::Point3f pointer_3d;
    cv::KalmanFilter kf_3d;
    cv::Mat prediction_3d;
    cv::Mat measurement_3d;
    cv::Mat estimation_3d;

    // 2D reprojected
    cv::Point2f pointer_2d;
    std::vector<cv::Point2f> pointerHistory;
    std::vector<cv::Point2f> gesturePoints;
    bool isGestureStarted;
    std::vector<DollarRecognizer::RecognitionResult> gestures;

    // track probability for garbage collector
    double prob;
};

class Tracker
{
public:
    Tracker() = delete;
    Tracker(const param_set & params, const Calibration & calibration) :
        m_params(&params), m_calibration(&calibration),
        m_lastTrackId(-1) { recognizer.loadTemplates(); }

    Track getCandidate(const cv::Mat & depth_undistorted,
                       const cv::Mat & point_cloud,
                       const std::vector<cv::Point> & cluster);

    void run(const cv::Mat & depth_undistorted,
             const cv::Mat & point_cloud,
             const std::vector<std::vector<cv::Point> > & clusters);

    void tryRecognize(Track & track);

    std::map<int, Track> tracks;

private:
    const param_set * m_params;
    const Calibration * m_calibration;
    DollarRecognizer::GeometricRecognizer recognizer;

    int m_lastTrackId;
};

#endif // TRACKER_HPP
