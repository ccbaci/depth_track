#ifndef MASK_H
#define MASK_H

#include <global.hpp>

/*
 * binary processing task
 * generates important pixels given confidence and depth image
 *
 *
 */

class Mask
{
public:
    Mask() = delete;
    Mask(const param_set & params) : m_params(&params) { }

    /*
     * input confidence and depth frames 16UC1
     * masks, saturates and invertes the depth frame
     * output depth mask 8UC1
     *
     */
    cv::Mat getDepthMask(const cv::Mat & confidence_undistorted,
                         const cv::Mat & depth_undistorted) {
        double min, max;
        cv::Mat confidence_mask, depth_mask;

        /* 8-bit representations */
        cv::minMaxLoc(confidence_undistorted, &min, &max);
        confidence_undistorted.convertTo(confidence_mask, CV_8UC1, 255. / max);
        cv::minMaxLoc(depth_undistorted, &min, &max);
        depth_undistorted.convertTo(depth_mask, CV_8UC1, 255. / max);

        /* masking */
        cv::Mat mask = depth_undistorted < m_params->maxDepth;
        cv::threshold(confidence_mask, confidence_mask, 20, 255, CV_THRESH_OTSU);

        cv::bitwise_and(mask, confidence_mask, mask);
        cv::bitwise_and(depth_mask, mask, depth_mask);
        cv::bitwise_not(depth_mask, depth_mask, mask);

        /* equalize hist for display purposes */
        cv::equalizeHist(depth_mask, depth_mask);

        return depth_mask;
    }

    /*
     * input depth mask 8UC1
     * outputs non zero pixels
     *
     */
    std::vector<cv::Point> getForeground(const cv::Mat & depth_mask) {
        std::vector<cv::Point> nonzeros;
        cv::findNonZero(depth_mask, nonzeros);
        return nonzeros;
    }

private:
    const param_set * m_params;
};

#endif // MASK_H
