#include <iostream>
#include <opencv2/opencv.hpp>

#include <calibration.hpp>
#include <mask.hpp>
#include <cluster.hpp>
#include <tracker.hpp>
#include <reader.hpp>

/*
 * main acts as the main processing thread using sub modules
 *
 *
 */

int main()
{

    // set parameters
    param_set params;
    params.fps = 30;

    // mask
    params.maxDepth = 2000;

    // clustering
    params.maxEuclidean = 30;
    params.minPoints = 20;

    // tracker
    params.minWidth = 70;
    params.maxWidth = 210;
    params.minArea = 500;
    params.searchRadius = 300;
    params.maxFramesLost = 10;
    params.maxTrackCount = 2;

    // kalman
    params.processNoise = .5;
    params.measurementNoise = 20;
    params.errorCovariance = 2;

    // gesture
    params.activityWindow = 20;
    params.activityThresh = 20;
    params.minGesturePoints = 30;
    params.minGestureScore = 0.7;
    params.minGestureVertice = 40;

    ////////////////////////////////

    camera_type camera = ds325;
    Reader reader(camera);
    reader.setRoot("/home/canberk/Desktop/assignment/dataset/");
    reader.selectDataset("gestures_two_hands_swap/");
    cv::VideoWriter writer;
    writer.open("/home/canberk/Desktop/gestures_two_hands_swap.mp4", CV_FOURCC('X', 'V', 'I', 'D'), 30, cv::Size(320, 240));

    Calibration calibrator(camera);
    Mask masker(params);
    Cluster cluster(params);
    Tracker tracker(params, calibrator);

    cv::Mat depth_mask;
    cv::Mat track_image;


    std::clock_t init = std::clock();

    int timestamp = 0;
    while(1) {
        try {
            std::clock_t start = std::clock();

            cv::Mat depth_16, confidence_16;
            cv::Mat point_cloud;

            std::cout << "-----------------------------------" << std::endl;

            /* read next images */
            depth_16 = reader.getDepth(timestamp);
            confidence_16 = reader.getConfidence(timestamp);

            /* end of folder */
            if(depth_16.empty() || confidence_16.empty()) break;

            /* undistort images */
            confidence_16 = calibrator.undistort(confidence_16);
            depth_16 = calibrator.undistort(depth_16);

            /* get 3D map */
            point_cloud = calibrator.getPointCloud(depth_16);

            /* get mask */
            depth_mask = masker.getDepthMask(confidence_16, depth_16);

            /* pyrdown for faster clustering */
            cv::Mat depth_pyr, depth_mask_pyr, pcl_pyr;
            cv::pyrDown(depth_16, depth_pyr);
            cv::pyrDown(depth_mask, depth_mask_pyr);
            cv::pyrDown(point_cloud, pcl_pyr);

            /* get foreground points */
            std::vector<cv::Point> nonzeros = masker.getForeground(depth_mask_pyr);
            if(nonzeros.size() == 0) throw -1;

            std::cout << "undist + mask : " << (std::clock() - start) * 1000 / (double) CLOCKS_PER_SEC << " ms" << std::endl;
            start = std::clock();

            /* get track positions */
            std::vector<cv::Point> oldTrackPoints;
            for(auto & T : tracker.tracks) {
                Track & track = T.second;
                if(track.state > Track::Verified)
                oldTrackPoints.push_back(track.pointer_2d * .5);
            }

            /* get clusters */
            std::vector<std::vector<cv::Point> > clusters = cluster.run(depth_pyr, pcl_pyr, nonzeros, oldTrackPoints);
            if(clusters.size() == 0) throw -2;

            /* show clusters with random unique colors */
            cv::RNG rng;
            cv::Mat clusters_image = cv::Mat::zeros(depth_mask.size(), CV_8UC3);
            for(int i = 0; i < clusters.size(); i++) {
                uchar b = 255 * rng.uniform(0.0, 1.0);
                uchar g = 255 * rng.uniform(0.0, 1.0);
                uchar r = 255 * rng.uniform(0.0, 1.0);
                for(int j = 0; j < clusters[i].size(); j++) {
                    clusters_image.at<cv::Vec3b>(clusters[i][j])[0] = b;
                    clusters_image.at<cv::Vec3b>(clusters[i][j])[1] = g;
                    clusters_image.at<cv::Vec3b>(clusters[i][j])[2] = r;
                }
            }

            std::cout << "cluster : " << (std::clock() - start) * 1000 / (double) CLOCKS_PER_SEC << " ms" << std::endl;
            start = std::clock();

            /* run tracker */
            tracker.run(depth_16, point_cloud, clusters);

            std::cout << "track : " << (std::clock() - start) * 1000 / (double) CLOCKS_PER_SEC << " ms" << std::endl;
            start = std::clock();

            /* show results */
            std::vector<std::vector<cv::Point> > contours;
            track_image = cv::Mat::zeros(depth_16.size(), CV_8UC3);
            for(auto & T : tracker.tracks) {
                Track & track = T.second;
                contours.push_back(track.hull);

                cv::Scalar color;
                switch(track.state) {
                case Track::Candidate :
                    color = cv::Scalar(40, 40, 40);
                    break;
                case Track::New :
                    color = cv::Scalar(127, 127, 127);
                    break;
                case Track::Verified :
                    color = cv::Scalar(0, 200, 0);
                    break;
                case Track::Lost :
                    color = cv::Scalar(0, 0, 127);
                    break;
                case Track::Deleted :
                    color = cv::Scalar(40, 40, 40);
                    break;
                default:
                    break;

                }

                cv::drawContours(track_image, contours, -1, color, 1);

                /* reproject pointer to sensor */
                cv::putText(track_image, std::to_string(track.id), track.pointer_2d, cv::FONT_HERSHEY_SIMPLEX, 0.5, color);

                cv::circle(track_image, track.pointer_2d, 2, color, 2);
                contours.clear();

                for(auto & p : track.gesturePoints) {
                    cv::circle(track_image, p, 1, cv::Scalar(200, 200, 0), 1);
                }

                if(track.gestures.size()) {
                    cv::putText(track_image, "   " +std::prev(track.gestures.end())->name, track.pointer_2d, cv::FONT_HERSHEY_SIMPLEX, 0.5, color);
                    for(auto & G : track.gestures) {
                        std::cout << track.id << " " << G.name << std::endl;
                    }
                }
            }

            cv::imshow("mask", depth_mask);
            cv::imshow("clusters", clusters_image);
            cv::imshow("tracks", track_image);

            writer.write(track_image);

            cv::waitKey(1);
            float elapsed_ms = (std::clock() - start) * 1000 / (double) CLOCKS_PER_SEC;
            if(elapsed_ms + 5 < 1000. / params.fps)
                cv::waitKey(1000. / params.fps - elapsed_ms);
            timestamp++;
        }
        catch(const int & e) {
            if(e == -1)
                std::cout << timestamp << " : bad mask" << std::endl;
            else if(e == -2)
                std::cout << timestamp << " : no cluster" << std::endl;
            ++timestamp;
        }
    }
    writer.release();

    std::cout << "----------------------" << std::endl;
    std::cout << "finished in " << (std::clock() - init) / (double) CLOCKS_PER_SEC << " seconds" << std::endl;
    std::cout << "avg fps " << 1. / (((std::clock() - init) / (double) CLOCKS_PER_SEC) / (double) timestamp) << std::endl;

    return 0;


}
