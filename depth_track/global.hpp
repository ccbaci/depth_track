#ifndef GLOBAL_HPP
#define GLOBAL_HPP

#include <vector>
#include <opencv2/opencv.hpp>
#include <omp.h>
#include <chrono>

/*
 * useful structs used
 *
 */

/* all parameters */
struct param_set {
    double fps;

    // max depth for binary masking
    double maxDepth;

    // min max hand width and area for track initiation
    double minWidth;
    double maxWidth;
    double minArea;

    // max number of tracks
    int maxTrackCount;

    // max distance and min neighbour points for dbscan
    double maxEuclidean;
    uint minPoints;

    // search radius for track assignment
    double searchRadius;

    // number of frames lost before track deletion
    int maxFramesLost;

    // kalman params
    double processNoise;
    double measurementNoise;
    double errorCovariance;

    // gesture activity decision window - number of frames
    int activityWindow;

    // gesture activity thresh in pixels
    double activityThresh;

    // min number of points collected before recognition
    int minGesturePoints;

    // min width or height of a gesture in pixels
    double minGestureVertice;

    // min recog score to decide a gesture
    double minGestureScore;
};

/* camera types */
enum camera_type {
    ds325,
    ds536
};

struct calib_data {
    /* camera params */
    double fx, fy;
    double fx_inv, fy_inv;
    double cx, cy;
    double k1, k2;
    double p1, p2;

    /* camera matrix */
    cv::Mat A;

    /* dist coeffs */
    std::vector<double> K;
};

#endif // GLOBAL_HPP
