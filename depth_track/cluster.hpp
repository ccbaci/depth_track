#ifndef CLUSTER_HPP
#define CLUSTER_HPP

#include <global.hpp>

/*
 * clusters given points into arbitrary number of regions
 * it outputs less than (2 * maxTrackCount) regions
 * priority to min depth and previous track regions
 *
 *
 */

class Cluster
{
public:
    Cluster() = delete;
    Cluster(const param_set & params) : m_params(&params) { }

    inline std::vector<std::vector<cv::Point> > run(const cv::Mat &depth,
                                                    const cv::Mat &point_cloud,
                                                    std::vector<cv::Point> &points,
                                                    std::vector<cv::Point> &oldTrackPoints) const {

        double epsilon_sqr = m_params->maxEuclidean * m_params->maxEuclidean;
        uint minPts = m_params->minPoints * .25;
        int n_max = m_params->maxTrackCount;

        /*
         * cluster voxels
         */
        std::vector<std::vector<cv::Point> > clusters;
        std::vector<bool> clustered; clustered.reserve(points.size());
        std::vector<bool> noised; noised.reserve(points.size());
        std::vector<bool> visited; visited.reserve(points.size());
        std::vector<int> neighborPts, neighborPts_;

        // sort points by depth
        std::sort(points.begin(), points.end(), [&] (const cv::Point &p1, const cv::Point &p2)
        { return depth.at<ushort>(p1) < depth.at<ushort>(p2); });

        // insert track points of last frame at beginning
        uint n_tracks = oldTrackPoints.size();
        if(n_tracks > 0) points.insert(points.begin(), oldTrackPoints.begin(), oldTrackPoints.end());

        // initialize clustered and visited
        for(auto p : points) {
            visited.push_back(false);
            noised.push_back(false);
            clustered.push_back(false);
        }

        // for each unvisted point P in points
        for(int i = 0; i < points.size(); i++) {
            // exit if max num of clusters reached
            if(n_tracks > 0 && clusters.size() >= 2 * n_tracks) break;
            else if(clusters.size() >= 2 * n_max) break;

            if(!visited[i]) {
                // mark P as visited
                visited[i] = true;
                neighborPts = m_regionCheck(point_cloud, points, points[i], epsilon_sqr);

                if(neighborPts.size() < minPts) {
                    // mark P as noise
                    noised[i] = true;
                }
                else {
                    // expand cluster
                    clusters.push_back(std::vector<cv::Point>());
                    std::prev(clusters.end())->reserve(points.size());
                    clustered[i] = true;

                    // add P to cluster c
                    std::prev(clusters.end())->push_back(points[i]);

                    // for each point P' in neighborPts
                    for(uint j = 0; j < neighborPts.size(); j++) {
                        // if P' is not visited
                        if(!visited[neighborPts[j]]) {
                            // mark P' as visited
                            visited[neighborPts[j]] = true;

                            // connect clusters if applicable
                            neighborPts_ = m_regionCheck(point_cloud, points, points[neighborPts[j]], epsilon_sqr);
                            if(neighborPts_.size() >= minPts) {
                                neighborPts.insert(neighborPts.end(), neighborPts_.begin(), neighborPts_.end());
                            }
                        }

                        // if P' is not yet a member of any cluster
                        // add P' to cluster c
                        if(!clustered[neighborPts[j]]) {
                            std::prev(clusters.end())->push_back(points.at(neighborPts[j]));
                            clustered[neighborPts[j]] = true;
                        }
                    }
                }
            }
        }

        for(auto & v : clusters) {
            for(auto & p : v) {
                p = p * 2;
            }
        }
        return clusters;

    }

private:
    const param_set * m_params;

    inline std::vector<int> m_regionCheck(const cv::Mat &image,
                                        const std::vector<cv::Point> &points,
                                        const cv::Point &point,
                                        const double & epsilon_sqr) const {
        std::vector<int> retKeys;
        double dist_sqr;

        retKeys.reserve(points.size());

#pragma omp for
        for(int i = 0; i < points.size(); ++i) {
            if(points[i] == point) continue;
            dist_sqr = pow((image.at<cv::Vec3s>(points[i])[0] - image.at<cv::Vec3s>(point)[0]), 2) +
                    pow((image.at<cv::Vec3s>(points[i])[1] - image.at<cv::Vec3s>(point)[1]), 2) +
                    pow((image.at<cv::Vec3s>(points[i])[2] - image.at<cv::Vec3s>(point)[2]), 2);
            if(dist_sqr <= epsilon_sqr) {
                retKeys.push_back(i);
            }
        }
        return retKeys;
    }

};

#endif // CLUSTER_HPP
